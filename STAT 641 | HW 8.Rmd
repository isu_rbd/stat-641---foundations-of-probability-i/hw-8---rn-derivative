---
title: "STAT 641 | HW 8"
author: "Ricardo Batista"
date: "11/13/2019"
output: pdf_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# Question 4.3

&nbsp;&nbsp;&nbsp;&nbsp; Let 

$$
B_n = \left\{x : h(x) > \frac{1}{n}\right\}.
$$

Since $h$ is integrable, 

$$
\infty > \int_{[0,1]} h d\nu \geq \int_{B_n} h d\nu \geq \frac{1}{n}\nu(B_n)
$$

For all $n$. Note that if $B_n$ is infinite, $\frac{1}{n}\nu(B_n) = \frac{1}{n} \cdot \infty = \infty$. Therefore, $B_n$ is finite for all $n$. Note that $B_n \uparrow B \equiv \{x: h(x) > 0\}$, so $B = \bigcup_{n \geq 1} B_n$. Thus, by m.f.c.b,

$$
\nu \left(B \right) = \lim_{n \rightarrow \infty} \nu(B_n) < \infty.
$$

by the limit inequality rule since $\nu(B_n) < \infty$ for every $n$.  

\pagebreak

# Question 4.4

## (i)

&nbsp;&nbsp;&nbsp;&nbsp; Given $\mu_2 \ll \mu_3$, 

$$
\begin{aligned}
\mu_3(A) = 0 \implies \mu_2(A) = 0 && \text{for all } A \in \mathcal{F}
\end{aligned}
$$

and since $\mu_1 \ll \mu_2$,

$$
\begin{aligned}
\mu_2(A) = 0 \implies \mu_1(A) = 0 && \text{for all } A \in \mathcal{F}
\end{aligned}
$$

then

$$
\begin{aligned}
\mu_3(A) = 0 \implies \mu_1(A) = 0 && \text{for all } A \in \mathcal{F}
\end{aligned}
$$

i.e., $\mu_1 \ll \mu_3$.  

&nbsp;&nbsp;&nbsp;&nbsp; Since $\mu_1 \ll \mu_2 \ll \mu_3$, we have that

$$
\mu_1(A) = \int_A \frac{d\mu_1}{d\mu_2} d\mu_2 = \int_A \frac{d\mu_1}{d\mu_3} d\mu_3
$$

for all $A \in \mathcal{F}$. Note that $\frac{d\mu_1}{d\mu_2}$ is a nonnegative measurable function. We seek to show that given $\mu_2 \ll \mu_3$, 

$$
\int_A \frac{d\mu_1}{d\mu_2} d\mu_2 = \int_A  \frac{d\mu_1}{d\mu_2} \frac{d\mu_2}{d\mu_3} d\mu_3.
$$


To do so, we prove the general case: given $\mu \ll \nu$ and $f$, a nonnegative measurable function,

$$
\int_\Omega fd\mu = \int_{\Omega} f\frac{d\mu}{d\nu} d\nu.
$$

We start with $f(\omega) = \mathbf{1}_A(\omega)$. Then,

$$
\int_\Omega \mathbf{1}_A(\omega)\mu(d\omega)
= \mu(A) 
= \int_A \frac{d\mu}{d\nu}(\omega) \nu(d\omega)
= \int_\Omega \mathbf{1}_A(\omega) \frac{d\mu}{d\nu}(\omega) \nu(d\omega).
$$

Note that the product of two measurable functions is also measurable -- a key fact that we will use throughout. Now consider $f (\omega) = \sum_{i = 1}^k c_i \mathbf{1}_{A_i}(\omega)$, so 

$$
\int_\Omega f d \mu
= \int_\Omega f (\omega) \mu(d \omega)
= \int_\Omega \sum_{i = 1}^k c_i \mathbf{1}_{A_i}(\omega) \mu(d \omega)
= \sum_{i = 1}^k c_i \mu(A_i)
$$

and

$$
\mu(A_i) = \int_{A_i} \frac{d \mu}{d \nu} d\nu.
$$

Then,

$$
\begin{aligned}
\int_{\Omega} \sum_{i = 1}^k c_i \mathbf{1}_{A_i}(\omega) \mu(d\omega)
&=  \sum_{i = 1}^k c_i \int_{\Omega} \mathbf{1}_{A_i}(\omega) \mu(d\omega)
= \sum_{i = 1}^k c_i \int_{A_i} \frac{d\mu}{d\nu}d\nu\\[10pt]
&= \sum_{i = 1}^k \int_{A_i} c_i\frac{d\mu}{d\nu}d\nu
= \int_{\Omega} \sum_{i = 1}^k  c_i \mathbf{1}_{A_i}(\omega) \frac{d\mu}{d\nu}(\omega) \nu(d\omega)\\[10pt]
&= \int_\Omega f(\omega) \frac{d\mu}{d\nu}(\omega) \nu(d\omega) 
=  \int_\Omega f \frac{d\mu}{d\nu} d\nu,
\end{aligned}
$$

so $\int_\Omega f d\mu = \int_\Omega f \frac{d\mu}{d\nu} d\nu$ for $f$ simple nonngegative. Now, recall that for nonnegative measurable function $f$ we can always construct a non-decreasing sequence $\{f_{ni}\}_{i \geq 1}$ of nonnegative simple functions such that $f_{ni}(\omega) \uparrow f_n(\omega)$ for all $\omega \in \Omega$. As such, by the definition of the integral of a nonnegative measurable function,

$$
\lim_{n \rightarrow \infty} \int_{\Omega} f_{ni} \frac{d\mu}{d\nu}d\nu = \int_{\Omega} f_{n} \frac{d\mu}{d\nu}d\nu.
$$

Finally, by MCT, for $\{f_n\}_{n \geq 1}$ nonngegative measurable such that $f_n(\omega) \uparrow f(\omega)$ for all $\omega \in \Omega$,

$$
\lim_{n \rightarrow \infty} \int_{\Omega} f_{n} \frac{d\mu}{d\nu}d\nu = \int_{\Omega} f \frac{d\mu}{d\nu}d\nu.
$$

Therefore, since $\mu_1 \ll \mu_2 \ll \mu_3$ and $\frac{d\mu_1}{d\mu_2}$ is a measurable nonnegative function, 

$$
\int_A \frac{d\mu_1}{d\mu_3} d\mu_3 = \int_A \frac{d\mu_1}{d\mu_2} d\mu_2 = \int_A  \frac{d\mu_1}{d\mu_2} \frac{d\mu_2}{d\mu_3} d\mu_3.
$$

so

$$
\frac{d\mu_1}{d\mu_3} = \frac{d\mu_1}{d\mu_2} \frac{d\mu_2}{d\mu_3}.
$$


\pagebreak

## (ii)

&nbsp;&nbsp;&nbsp;&nbsp; First note that since $\alpha \geq 0$, we have

- (i) $\alpha\mu_1(A) \in [0, \infty]$ for all $A \in \mathcal{F}$
- (ii) $\alpha\mu_1(\emptyset) = \alpha \cdot 0 = 0$
- (iii) $\left(\{A_n\}_{n \geq 1} \subset \mathcal{F} \right)(A_i \cap A_j = \emptyset \text{ for } i \neq j) \implies \alpha\mu_1(\bigcup_{n \geq 1} A_n) = \sum_{n \geq 1} \alpha \mu(A_n)$

i.e., $\alpha \mu_1$ is a measure. Now, since $\mu_3(A) = 0 \implies \mu_1(A) = 0$, for $\alpha \geq 0$,

$$
\mu_3(A) = 0 \implies \alpha\mu(A) = \alpha \cdot 0 = 0,
$$

i.e., $\mu_1 \ll \mu_3 \implies \alpha\mu_1 \ll \mu_3$. Next consider

$$
\begin{aligned}
\mu_1(A) &= \int_A \frac{d\mu_1}{d \mu_3} d\mu_3\\[10pt]
\alpha \mu_1(A) &= \alpha\int_A  \frac{d\mu_1}{d \mu_3} d\mu_3
= \int_A  \alpha\frac{d\mu_1}{d \mu_3} d\mu_3
\end{aligned}
$$

for all $A \in \mathcal{F}$. So, since $\alpha \mu_1 \ll \mu_3$ and $\alpha\frac{d\mu_1}{d \mu_3}$ is a nonnegative measurable function, then, by the RN theorem, $\alpha\frac{d\mu_1}{d \mu_3}$ is the RN derivative of $\alpha \mu_1$ w.r.t. $\mu_3$. Similar results hold for $\beta \mu_2$. Therefore, we have

$$
\begin{aligned}
(\alpha \mu_1 + \beta \mu_2) (A) 
&= \alpha \mu_1 (A) + \beta \mu_2 (A)\\[10pt]
&= \int_A \alpha\frac{d\mu_1}{d \mu_3} d\mu_3 + 
   \int_A \beta\frac{d\mu_2}{d \mu_3} d\mu_3\\[10pt]
&= \int_A \alpha\frac{d\mu_1}{d \mu_3} + \beta\frac{d\mu_2}{d \mu_3} d\mu_3
&& \text{linearity prop. of nonneg. meas. funs.}
\end{aligned}
$$

for all $A \in \mathcal{F}$. That is,

$$
\alpha\frac{d\mu_1}{d \mu_3} + \beta\frac{d\mu_2}{d \mu_3} \text{ is the RN derivative of } \alpha \mu_1 + \beta \mu_2 \text{ w.r.t. } \mu_3.
$$

\pagebreak

## (iii)

&nbsp;&nbsp;&nbsp;&nbsp; Recall that $\mu \ll \nu$ means

$$
\nu(A) = 0 \implies \mu(A) = 0.
$$

Now, since $\frac{d \mu}{d \nu} > 0$ a.e. ($\nu$),

$$
\mu(A) = \int_A \frac{d \mu}{d \nu} d\nu = 0
$$

only if $\nu(A) = 0$ since $\nu\left(\left\{ \omega \in \Omega : \frac{d \mu}{d \nu}(\omega) \leq 0 \right\}\right) = 0$. In other words, $\mu \gg \nu$. Then, by the RN theorem,

$$
\begin{aligned}
\nu(A) = \int_A \frac{d\nu}{d\mu} d\mu, && (1)
\end{aligned}
$$

&nbsp;&nbsp;&nbsp;&nbsp; Now note that since $\frac{d \mu}{d \nu} > 0$, $\left(\frac{d \mu}{d \nu}\right)^{-1}$ is defined everywhere (i.e., there is no risk of dividing by 0). Then we have

$$
\begin{aligned}
\nu(A) 
&= \int_A d\nu 
= \int_{\Omega} \boldsymbol{1}_A \left(\frac{d \mu}{d \nu}\right)^{-1} \frac{d \mu}{d \nu} d\nu
\end{aligned}
$$

Also realize that since $\frac{d \mu}{d \nu} > 0$, $\left(\frac{d \mu}{d \nu}\right)^{-1} > 0$ as well. Then, by part (i), since $\mu \ll \nu$,

$$
\begin{aligned}
\nu(A) 
&= \int_{\Omega} \boldsymbol{1}_A \left(\frac{d \mu}{d \nu}\right)^{-1} d\mu
= \int_{A} \left(\frac{d \mu}{d \nu}\right)^{-1} d\mu. && (2)
\end{aligned}
$$

Then, by (1) and (2), and the uniqueness of the RN derivative,

$$
\begin{aligned}
\frac{d\nu}{d\mu} = \left(\frac{d \mu}{d \nu}\right)^{-1} && \text{a.e. } (\mu).
\end{aligned}
$$

\pagebreak

## (iv)

### (a)

&nbsp;&nbsp;&nbsp;&nbsp; $\underline{(\implies)}$

&nbsp;&nbsp;&nbsp;&nbsp; By definition of $\mu \ll \nu$ we have that

$$
\nu(A) = 0 \implies \mu(A) 
= \left(\sum_{n = 1}^{\infty} \alpha_n \mu_n\right)(A) 
= \sum_{n = 1}^{\infty} \alpha_n \mu_n(A) = 0.
$$

Recall that the $\alpha_n$ are *positive* and the measures nonnegative, so 

$$
\sum_{n = 1}^{\infty} \alpha_n \mu_n(A) = 0 \implies \mu_n(A) = 0 \text{ for all } n\geq 1,
$$

i.e., $\mu_n \ll \nu$ for each $n \geq 1$. Next, by the RN theorem we have that

$$
\mu(A) = \int_A \frac{d \mu}{ d\nu} d\nu.
$$

Moreover, since each $\alpha_n\mu_n$ is a measure (by part (ii)) dominated by $\nu$, we have that

$$
\begin{aligned}
\alpha_n\mu_n(A) = \int \alpha_n\frac{d\mu_n}{d\nu}d\nu
\end{aligned}
$$

by part (ii). Then,

$$
\begin{aligned}
\sum_{n = 1}^{\infty} \alpha_n \mu_n(A) 
&= \sum_{n = 1}^{\infty} \int_A \alpha_n\frac{d\mu_n}{d\nu}d\nu\\[10pt]
&=  \int_A \sum_{n = 1}^{\infty} \alpha_n\frac{d\mu_n}{d\nu} d\nu
\end{aligned}
$$

where the last equality follows by Corollary 2.3.5 since the $\alpha_n\frac{d\mu_n}{d\nu}$ are nonnegative measurable functions. Finally, by the uniqueness of the RN derivative,

$$
\begin{aligned}
\frac{d \mu}{ d\nu} = \sum_{n = 1}^{\infty} \alpha_n\frac{d\mu_n}{d\nu} && \text{a.e. } (\nu). 
\end{aligned}
$$

&nbsp;&nbsp;&nbsp;&nbsp; $\underline{(\impliedby)}$

&nbsp;&nbsp;&nbsp;&nbsp; By definition, $\mu_n \ll \nu$ for each $n \geq 1$ means

$$
\nu(A) = 0 \implies \mu_n(A) = 0,
$$

so it is easy to see that

$$
\nu(A) = 0 \implies \sum_{n = 1}^{\infty} \alpha_n \mu_n(A) = 0.
$$

### (b)

&nbsp;&nbsp;&nbsp;&nbsp; $\underline{(\implies)}$

&nbsp;&nbsp;&nbsp;&nbsp; Recall that $\mu \perp \nu$ means there exists $B \in \mathcal{F}$ such that $\mu(B) = 0$ and $\nu(B^c) = 0$. Note that

$$
\begin{aligned}
0 = \mu(B) 
= \left(\sum_{n = 1}^{\infty} \alpha_n \mu_n\right)(B) 
= \sum_{n = 1}^{\infty} \alpha_n \mu_n(B)
\end{aligned}
$$

implies that $\mu_n(B) = 0$ for each $n \geq 1$ since the $a_n$ are positive, i.e., $\mu_n \perp \nu$.  

&nbsp;&nbsp;&nbsp;&nbsp; $\underline{(\impliedby)}$

&nbsp;&nbsp;&nbsp;&nbsp; Note that $\mu_n \perp \nu$ means that there exist $B_n \in \mathcal{F}$ such that $\mu_n(B_n) = 0$ and $\nu(B_n^c) = 0$. Let $B = \bigcap_{n = 1}^{\infty} B_n$ and $B^c = \bigcap_{n = 1}^{\infty} B_n^c$. Then, 

$$
\begin{aligned}
\mu(B) 
= \left(\sum_{n = 1}^{\infty} \alpha_n \mu_n\right)(B) 
= \sum_{n = 1}^{\infty} \alpha_n \mu_n(B) = 0 &&
\text{and} &&
\nu(B^c) = 0,
\end{aligned}
$$

which makes $B \in \mathcal{F}$ the set that meets the singular criteria.  

\pagebreak

# Question 4.5

## (a)

### Lebesgue decomposition

&nbsp;&nbsp;&nbsp;&nbsp; Recall that the support of Normal and Exponential random variables are $(-\infty, \infty)$ and $[0, \infty)$, respectively. Then, let

$$
\begin{aligned}
\mu_a(A) = N(0, 1) \text{ of } A \cap [0, \infty) && \text{and} && 
\mu_s(A) = N(0, 1) \text{ of } A \cap (-\infty, 0).
\end{aligned}
$$

Note that for $B \equiv (-\infty, 0)$,

$$
\begin{aligned}
\nu(B) &= \nu((-\infty, 0)) = 0 \\[10pt]
\mu_s(B^c) &= \mu_s([0, \infty)) = \mu((-\infty, 0) \cap [0, \infty)) =  \mu(\emptyset) =0
\end{aligned}
$$

so $\mu_s \perp \nu$. 

&nbsp;&nbsp;&nbsp;&nbsp; Now note that $\nu(A) = 0$ whenever $A \subset (\infty, 0)$ or $A$ is countable. When $A \subset (\infty, 0)$, $\mu_a(A) = \mu(A \cap [0, \infty)) = \mu(\emptyset) = 0$. Similarly, when $A$ is countable $\nu(A) = \mu_a(A) = 0$. Therefore,

$$
\begin{aligned}
\nu(A) = 0 \implies \mu_a(A) = 0.
\end{aligned}
$$

### Radon-Nikodym derivative

&nbsp;&nbsp;&nbsp;&nbsp; We seek $h = \frac{d \mu_a}{d\nu}$ such that

$$
\begin{aligned}
\mu_a(A) = \int_A h d\nu = \int_A \frac{d \mu_a}{d\nu} d\nu  && \text{for all } A \in \mathcal{F}.
\end{aligned}
$$

Let 

$$
\begin{aligned}
m(A) &\equiv \text{Lebesgue measure}\\[10pt]
m_{+}(A) &\equiv \text{Lebesgue measure of } A \cap [0, \infty),
\end{aligned}
$$

and note that $\mu_a \ll m_+$. Then we have

$$
\mu_a(A) = \int_A h_{\mu_a} dm_{+} = \int_A \frac{d \mu_a}{d m_{+}} dm_{+} 
= \int_{A \cap [0, \infty)} \frac{1}{\sqrt{2 \pi}} \exp\left\{-\frac{x^2}{2}\right\} dx
$$

for all $A \in \mathcal{F}$. Similarly, $\nu \ll m_+$ and

$$
\nu(A) = \int_A h_{\nu} dm_{+} = \int_A \frac{d \nu}{d m_{+}} dm_{+} 
= \int_{A \cap [0, \infty)} \exp\left\{-x\right\} m(dx)
$$

for all $A \in \mathcal{F}$. Now, note that $h_{\nu} > 0$ a.e. $(m_{+})$. So, by Proposition 4.1.2 we have that $\nu \gg m_+$ and

$$
\frac{dm_+}{d\nu} = \left(\frac{d\nu}{dm_{+}}\right)^{-1} 
= \left(\exp\left\{-x\right\}\right)^{-1}
= \exp\left\{x\right\}.
$$

Therefore,

$$
\begin{aligned}
\frac{d \mu_a}{d\nu} =  \frac{d \mu_a}{d m_{+}} \frac{dm_+}{d\nu} 
= \frac{1}{\sqrt{2 \pi}} \exp\left\{-\frac{x^2}{2}\right\}  \exp\left\{x\right\}
= \frac{1}{\sqrt{2 \pi}} \exp\left\{-\frac{x^2}{2} + x\right\} \hspace{15pt} a.e. (\nu).
\end{aligned}
$$


\pagebreak

## (b)

### Lebesgue decomposition

&nbsp;&nbsp;&nbsp;&nbsp; Note that since the support of a Normal r.v. is a subset of that of an Exponential and the pdf of a $N(0,1)$ is always positive, $\nu(A) = 0$ already implies $\mu(A) = 0$. Thus, 

$$
\begin{aligned}
\mu_a (A) = \mu
&& \text{and} &&
\mu_s (A) = 0.
\end{aligned}
$$

Since $\mu_s$ is the zero measure, the singularity criteria is met by letting $B$ be any element in $\mathcal{F}$. 

### Radon-Nikodym derivative

&nbsp;&nbsp;&nbsp;&nbsp; We proceed as in part (a). Note that, since $\mu_a \ll m$,

$$
\begin{aligned}
\mu_a(A) 
= \int_A \frac{d\mu}{dm} dm 
= \int_A  \exp \left\{-x\right\}I_{(0, \infty)} (x) m(dx)
\end{aligned}
$$

and, since $\nu \ll m$

$$
\begin{aligned}
\nu(A) 
= \int_A \frac{d\nu}{dm} dm 
= \int_A \frac{1}{\sqrt{2 \pi}} \exp \left\{-\frac{x^2}{2}\right\} m(dx).
\end{aligned}
$$

Moreover, since $\frac{d\nu}{dm} > 0$ a.e. $(m)$, then $m \ll \nu$ and

$$
\frac{dm}{d\nu} = \left( \frac{d\nu}{dm}\right)^{-1} 
= \sqrt{2 \pi} \exp \left\{\frac{x^2}{2}\right\}.
$$

Finally,

$$
\begin{aligned}
\frac{d\mu_a}{d \nu} 
= \frac{d\mu}{d \nu} 
= \frac{d\mu}{d m} \frac{dm}{d\nu}
&= \exp \left\{-x\right\} I_{(0, \infty)} (x) 
\sqrt{2 \pi} \exp \left\{\frac{x^2}{2}\right\}\\[10pt]
&= \sqrt{2 \pi} \exp \left\{-x  + \frac{x^2}{2}\right\} I_{(0, \infty)} (x) \hspace{15pt} a.e. (\nu).
\end{aligned}
$$

\pagebreak

## (c)

### Lebesgue decomposition

&nbsp;&nbsp;&nbsp;&nbsp; Recall that Poisson and Cauchy r.v.s have support $\mathbb{N_0}$ and $\mathbb{R}$, respectively. Let

$$
\begin{aligned}
\mu_a(A) &\equiv \mu \text{ of } A \cap (\mathbb{R} \setminus \mathbb{N_0}) = \mu_1 (A)\\[10pt]
\mu_s(A) &\equiv \mu \text{ of } A \cap \mathbb{N_0} = \mu_2(A)
\end{aligned}
$$

Since both Cauchy and Normal r.v.s have the same support and are discrete, $\mu_a = \mu_1 \ll \nu$. Notice that by setting $B \equiv \mathbb{N}$,

$$
\begin{aligned}
\nu(B) &= 0 && \text{since Cauchy r.v. is continuous}\\[10pt]
\mu_s(B^c) &= \mu_2(\mathbb{R} \setminus \mathbb{N}_0) = 0 && \text{since Poisson r.v. is discrete}
\end{aligned}
$$

which fulfills the singularity requirement.  

### Radon-Nikodym derivative

&nbsp;&nbsp;&nbsp;&nbsp; By our work in (a)-(b), we know that $\mu_a \ll m$ and are aware of $\frac{d\mu_a}{dm}$. By similar reasoning, it is clear that $\nu \ll m$ and, since $\frac{d\nu}{dm}$ a.e. ($m$), $\nu \gg m$. Therefore, noting 

$$
\nu(A) = \int_A \frac{d\nu}{dm} dm = \int_A \frac{1}{\pi} \frac{1}{1 + x^2} m (dx)
$$

we have that

$$
\begin{aligned}
\frac{d\mu_a}{d\nu} = \frac{d\mu_1}{d\nu} 
= \frac{d\mu_1}{dm} \left(\frac{d\nu}{dm}\right)^{-1}
&= \frac{1}{\sqrt{2 \pi}} \exp \left\{-\frac{x^2}{2}\right\} \left( \frac{1}{\pi} \frac{1}{1 + x^2} \right)^{-1}\\[10pt]
&= \frac{1}{\sqrt{2 \pi}} \exp \left\{-\frac{x^2}{2}\right\} \pi(1 + x^2)\\[10pt]
&= \frac{\sqrt{\pi}(1 + x^2)}{\sqrt{2}} \exp \left\{-\frac{x^2}{2}\right\} \hspace{15pt} a.e. (\nu).
\end{aligned}
$$

\pagebreak

## (d)

### Lebesgue decomposition

&nbsp;&nbsp;&nbsp;&nbsp; Recall that the support of a Geometric r.v. is $\mathbb{N_0}$. Let

$$
\begin{aligned}
\mu_a(A) &\equiv \mu \text{ of } A \cap \mathbb{N_0} = \mu_2(A)\\[10pt]
\mu_s(A) &\equiv \mu \text{ of } A \cap (\mathbb{R} \setminus \mathbb{N}_0) = \mu_1(A)
\end{aligned}
$$

Since Geometric and Poisson r.v.s share the same support, $\mu_a = \mu_2 \ll \nu$. Now note that by setting $B \equiv \mathbb{R} \setminus \mathbb{N}_0$,

$$
\begin{aligned}
\nu(B) &= 0\\[10pt]
\mu_s(B^c) &= \mu_1(\mathbb{N}_0) = 0
\end{aligned}
$$
we fulfill the singularity requirement.  

### Radon-Nikodym derivative

&nbsp;&nbsp;&nbsp;&nbsp; Let 

$$
\mu_c^+ (A) \equiv \text{counting measure of } A \cap \mathbb{N}_0.
$$

Clearly, $\mu_a = \mu_2 \ll \mu_c^+$ and $\nu \ll \mu_c^+$. Recall that

$$
\begin{aligned}
\mu_2(A) 
= \int_A \frac{d \mu_2}{d\mu_c^+} d\mu_c^+
&= \int_A \sum_{i = 0}^{\infty}\frac{e^{-1}}{i!} \boldsymbol{1}_{\{i\}}(x)\mu_c^+(dx) &&
\text{where } i \in \mathbb{N}_0
\end{aligned}
$$

and

$$
\begin{aligned}
\nu(A) 
= \int_A \frac{d \nu}{d\mu_c^+} d\mu_c^+
= \int_A \sum_{i = 0}^{\infty} p(1 - p)^i \boldsymbol{1}_{\{i\}}(x) \mu_c^+(dx)  &&
\text{where } i \in \mathbb{N}_0
\end{aligned}
$$


Moreover, since $\frac{d\mu_2}{d\mu_c^+} > 0$ a.e. ($\mu_c^+$), $\mu_c^+ \ll \mu_2$. Therefore,

$$
\begin{aligned}
\frac{d\mu_a}{d\nu} = \frac{d\mu_2}{d\nu} 
= \frac{d\mu_2}{d\mu_c^+} \left(\frac{d\nu}{d\mu_c^+}\right)^{-1}
&= \left(\sum_{i = 0}^{\infty}\frac{1}{i!e} \boldsymbol{1}_{\{i\}}(x)\right)
  \left(\sum_{i = 0}^{\infty} p(1 - p)^i \boldsymbol{1}_{\{i\}}(x)\right)^{-1}\\[10pt]
&= \left(\sum_{i = 0}^{\infty}\frac{1}{i!e} \boldsymbol{1}_{\{i\}}(x)\right)
   \left(\sum_{i = 0}^{\infty} \frac{1}{p(1 - p)^i} \boldsymbol{1}_{\{i\}}(x)\right)\\[10pt]
&= \sum_{i = 0}^{\infty} \frac{1}{i!ep(1 - p)^i} \boldsymbol{1}_{\{i\}}(x) \hspace{15pt} a.e. (\nu)
\end{aligned}
$$

where, as above, $i \in \mathbb{N}_0$.  

\pagebreak

## (e)


### Lebesgue decomposition

&nbsp;&nbsp;&nbsp;&nbsp; Let

$$
\begin{aligned}
\mu_a &\equiv \mu = \mu_1 + \mu_2\\[10pt]
\mu_s &\equiv 0.
\end{aligned}
$$

Note that $\nu(A) = 0$ implies $A = \emptyset$ or $A \subset \mathbb{Z}^-$. In either case, since $\mu$ and $\nu$ share support (i.e., $\mathbb{N}_0$), $\mu(A) = 0$. That is $\mu_a = \mu \ll \nu$. The, as in part (b), since $\mu_s$ is the zero measure, the singularity criteria is met by letting $B$ be any element in $\mathcal{F}$. Moreover, from our work above note that $\mu_1 \ll \nu_1$ and $\mu_2 \ll \nu_2$.  

### Radon-Nikodym derivative

&nbsp;&nbsp;&nbsp;&nbsp; Now, since $\mu \ll \nu$, we have that

$$
\begin{aligned}
\mu(A) &= (\mu_1 + \mu_2)(A) = \mu_1(A) + \mu_2(A)
= \int_A \frac{d\mu_1}{d\nu_1} d\nu_1 +
  \int_A \frac{d\mu_2}{d\nu_2} d\nu_2\\[10pt]
&= \int_A \frac{d\mu_1}{d\nu_1} \mathbf{1}_{\mathbb{N}^c}d\nu_1 +
   \int_A \frac{d\mu_1}{d\nu_1} \mathbf{1}_{\mathbb{N}}d\nu_1 +
   \int_A \frac{d\mu_2}{d\nu_2} \mathbf{1}_{\mathbb{N}^c}d\nu_2 +
   \int_A \frac{d\mu_2}{d\nu_2} \mathbf{1}_{\mathbb{N}}d\nu_2\\[10pt]
&= \int_A \frac{d\mu_1}{d\nu_1} \mathbf{1}_{\mathbb{N}^c}d(\nu_1 + \nu_2) + 
   \int_A \frac{d\mu_2}{d\nu_2} \mathbf{1}_{\mathbb{N}}d(\nu_1 + \nu_2)\\[10pt]
&= \int_A \frac{d\mu_1}{d\nu_1} \mathbf{1}_{\mathbb{N}^c} + 
          \frac{d\mu_2}{d\nu_2} \mathbf{1}_{\mathbb{N}} d(\nu_1 + \nu_2)
\end{aligned}
$$

which shows that 

$$
\frac{d\mu_1}{d\nu_1} \mathbf{1}_{\mathbb{N}^c} + \frac{d\mu_2}{d\nu_2} \mathbf{1}_{\mathbb{N}}
= \frac{d(\mu_1 + \mu_2)}{d(\nu_1 + \nu_2)}.
$$

Now, from our work above 

Moreover, since $\frac{d\mu_2}{d\mu_c^+} > 0$ a.e. ($\mu_c^+$), $\mu_c^+ \ll \mu_2$. Therefore,

$$
\begin{aligned}
\frac{d\mu_a}{d\nu} = \frac{d\mu}{d\nu}
= \frac{d(\mu_1 + \mu_2)}{d(\nu_1 + \nu_2)} 
&= \left(\frac{d(\mu_1 + \mu_2)}{d(m + \mu_c^+)} \right)
  \left(\frac{d(\nu_1 + \nu_2)}{d(m + \mu_c^+)} \right)^{-1}\\[10pt]
&= \left(\frac{d\mu_1}{dm}\mathbf{1}_{\mathbb{N}^c} + \frac{d\mu_2}{d\mu_c^+}\mathbf{1}_{\mathbb{N}}\right)
  \left(\frac{d\nu_1}{dm}\mathbf{1}_{\mathbb{N}^c} + \frac{d\nu_2}{d\mu_c^+}\mathbf{1}_{\mathbb{N}}\right)^{-1}\\[10pt]
&= \left(\frac{d\mu_1}{dm} \left(\frac{d\nu_1}{dm}\right)^{-1}\right)\mathbf{1}_{\mathbb{N}^c} + 
   \left( \frac{d\mu_2}{d\mu_c^+}\left(\frac{d\nu_2}{d\mu_c^+}\right)^{-1}\right)\mathbf{1}_{\mathbb{N}}\\[10pt]
&= \frac{\sqrt{\pi}(1 + x^2)\exp\left\{-\frac{x}{2}\right\}}{\sqrt{2}}\mathbf{1}_{\mathbb{N}^c}(x) +
   \sum_{i = 1}^{\infty}\frac{1}{ei!p(1 - p)^i}\mathbf{1}_{\mathbb{N}}(x)
   \hspace{15pt} \text{a.e.} (\nu)
\end{aligned}
$$

where, as above, $i \in \mathbb{N}_0$.  

\pagebreak

## (f)

### Lebesgue decomposition

&nbsp;&nbsp;&nbsp;&nbsp; Note the support of the Binomial r.v. is $[10] \equiv\{0, 1, ..., 10\}$. Then, let

$$
\begin{aligned}
\mu_a (A) &\equiv \mu(A)\\[10pt]
\mu_s (A) &\equiv \mu \left(A \cap (\mathbb{R} \setminus [10])\right) = 0.\\[10pt]
\end{aligned}
$$

Note that $\nu(A) = 0$ when $A \not\subset \mathbb{N}_0$; thus, $\nu(A) = 0 \implies \mu(A) = 0$. Next, since $\mu_s$ is the zero measure, any $B \in \mathcal{F}$ satisfies the singularity condition.   

### Radon-Nikodym derivative

&nbsp;&nbsp;&nbsp;&nbsp; Let $\mu_c^{10} \equiv \text{counting measure on [10]}$. Clearly $\mu \ll \mu_c^{10}$ and we have

$$
\begin{aligned}
\mu(A) = \int_A \frac{d\mu}{d\mu_c^{10}} d\mu_c^{10}
&= \int_A \sum_{i = 0}^{10} \begin{pmatrix} 10 \\ i\end{pmatrix} \left(\frac{1}{2}\right)^i\left(1 - \frac{1}{2}\right)^{10 - i} \boldsymbol{1}_{\{i\}}(x) \mu_c^{10}(dx)\\[10pt]
&= \int_A \sum_{i = 0}^{10} \begin{pmatrix} 10 \\ i\end{pmatrix} \left(\frac{1}{2}\right)^{10} \boldsymbol{1}_{\{i\}}(x) \mu_c^{10}(dx) \hspace{15pt} \text{where } i \in [10].
\end{aligned}
$$

From above, we know that $\nu \ll \mu_c^{10}$. Moreover, since $\frac{d\mu}{d\mu_c^{10}} > 0$ a.e. ($\mu_c^{10}$), $\mu_c^{10} \ll \mu$. Therefore, we have that

$$
\begin{aligned}
\frac{d\mu_a}{d\nu} = \frac{d\mu}{d\nu}
&= \frac{d\mu}{d\mu_c^{10}} \left(\frac{d\nu}{d\mu_c^{10}}\right)^{-1} \\[10pt]
&= \left(\sum_{i = 0}^{10} \begin{pmatrix} 10 \\ i\end{pmatrix} \left(\frac{1}{2}\right)^{10} \boldsymbol{1}_{\{i\}}(x)\right)
\left(\sum_{i = 0}^{\infty}\frac{1}{i!e} \boldsymbol{1}_{\{i\}}(x)\right)^{-1} \\[10pt]
&= \sum_{i = 0}^{10} \begin{pmatrix} 10 \\ i\end{pmatrix} \left(\frac{1}{2}\right)^{10} i!e \boldsymbol{1}_{\{i\}}(x) \hspace{15pt} \text{a.e.} (\nu)
\end{aligned}
$$

where $i \in [10]$.  

\pagebreak

# Extra question

## (a)

&nbsp;&nbsp;&nbsp;&nbsp; $\underline{(i)}$  

&nbsp;&nbsp;&nbsp;&nbsp; Since $\mu_1(A) \geq 0$ and $\mu_2(A) \geq 0$ for all $A \in \mathcal{F}$, then

$$
\begin{aligned}
\mu_3(A) = \mu_1(A) + \mu_2(A) \geq 0.
\end{aligned}
$$

&nbsp;&nbsp;&nbsp;&nbsp; $\underline{(ii)}$  

&nbsp;&nbsp;&nbsp;&nbsp; Since $\mu_1(\emptyset) = \mu_2(\emptyset) = 0$, $\mu_3(\emptyset) = \mu_1(\emptyset) + \mu_2(\emptyset) = 0$.


&nbsp;&nbsp;&nbsp;&nbsp; $\underline{(iii)}$  

&nbsp;&nbsp;&nbsp;&nbsp; Let $\{A_n\}_{n \geq 1} \subset \mathcal{F}$ be disjoint, then, since $\mu_1$ and $\mu_2$ are measures, 

$$
\begin{aligned}
\mu_1 \left(\bigcup_{n \geq 1} A_n\right) = \sum_{n \geq 1} \mu_1 (A_n) && \text{and} &&
\mu_2 \left(\bigcup_{n \geq 1} A_n\right) = \sum_{n \geq 1} \mu_2 (A_n).
\end{aligned}
$$

Then

$$
\begin{aligned}
\mu_3 \left(\bigcup_{n \geq 1} A_n\right) &=
\mu_1 \left(\bigcup_{n \geq 1} A_n\right)  + \mu_2 \left(\bigcup_{n \geq 1} A_n\right)\\[10pt]
&=\sum_{n \geq 1} \mu_1 (A_n) + \sum_{n \geq 1} \mu_2 (A_n)\\[10pt]
&= \sum_{n \geq 1} \mu_1 (A_n) + \mu_2 (A_n) 
= \sum_{n \geq 1} \mu_3 (A_n)
\end{aligned}
$$

## (b)

&nbsp;&nbsp;&nbsp;&nbsp; Since $\mu_1 \ll \nu$ and $\mu_2 \ll \nu$, we have that 

$$
\begin{aligned}
\nu(A) = 0 \implies \mu_1(A) = 0 && \text{and} &&
\nu(A) = 0 \implies \mu_2(A) = 0 && 
\text{for all } A \in \mathcal{F}.
\end{aligned}
$$

Thus we have that

$$
\nu(A) = 0 \implies \mu_1(A) + \mu_2(A) = \mu_3(A) = 0.  
$$

\pagebreak

## (c)

&nbsp;&nbsp;&nbsp;&nbsp; Note that since $\mu_3 \ll \nu$, we have (by RN theorem)

$$
\begin{aligned}
\mu_3(A) = \int_A h_3 d\nu && \text{for all } A \in \mathcal{F}
\end{aligned}
$$

where

$$
h_3 = \frac{d \mu_3}{d \nu}.
$$

Similarly, since $\mu_1 \ll \nu$ and $\mu_2 \ll \nu$,

$$
\begin{aligned}
\mu_1(A) = \int_A h_1 d\nu && \text{and} &&
\mu_2(A) = \int_A h_2 d\nu &&
\text{for all } A \in \mathcal{F}
\end{aligned}
$$

where

$$
\begin{aligned}
h_1 = \frac{d \mu_1}{d \nu} && \text{and} && h_2 = \frac{d \mu_2}{d \nu}.
\end{aligned}
$$

Therefore,

$$
\begin{aligned}
\mu_3(A) &= \mu_1(A) + \mu_2(A)\\[10pt]
&= \int_A h_1 d\nu  + \int_A h_2 d\nu\\[10pt]
&= \int_A \frac{d \mu_1}{d \nu} d\nu + \int_A \frac{d \mu_2}{d \nu} d\nu\\[10pt]
&= \int_A \left(\frac{d \mu_1}{d \nu} + \frac{d \mu_2}{d \nu} \right) d\nu  && \text{for all } A \in \mathcal{F}.
\end{aligned}
$$

where the last step followed by the linearity property of nonnegative measurable functions (since both $h_1$ and $h_2$ are nonnegative measurable functions). Thus, 

$$
\frac{d\mu_3}{d \nu} = \frac{d \mu_1}{d \nu} + \frac{d \mu_2}{d \nu}.
$$

\pagebreak

## (d)

&nbsp;&nbsp;&nbsp;&nbsp; Recall that $\mu \ll \nu$ is defined as

$$
\begin{aligned}
\nu(A) = 0  \implies \mu(A) = 0 && \text{for all } A \in \mathcal{F},
\end{aligned}
$$

and $\mu \perp \nu$ means there exist some $B \in \mathcal{F}$ such that

$$
\nu(B) = 0 \implies \mu(B^c) = 0.
$$

But by $\mu \ll \nu$

$$
\nu(B) = 0 \implies \mu(B) = 0
$$

and since $B \cup B^c = \Omega$ and measures are nonnegative, either $\mu$ is the zero measure or $\Omega = \emptyset$.  

\pagebreak

## (e)

### Lebesgue decomposition

&nbsp;&nbsp;&nbsp;&nbsp; Recall that Poisson and Geometric r.v.s share the same support (i.e., $\mathbb{N}_0$, and the support of a Lognormal r.v. is $(0, \infty)$. Then, let

$$
\begin{aligned}
\mu_{3a}(A) &= \mu_3(A \cap \mathbb{N}_0) = \mu_2(A)\\[10pt]
\mu_{3s}(A) &= \mu_3( A \cap (\mathbb{R} \setminus \mathbb{N}_0)) = \mu_1(A).
\end{aligned}
$$

Note that for $B \equiv \mathbb{R} \setminus \mathbb{N}$,

$$
\begin{aligned}
\nu(B) &= \nu(\mathbb{R} \setminus \mathbb{N}_0) = 0 \\[10pt]
\mu_{3s}(B^c) &= \mu_{3s}(\mathbb{N}_0 \cap (\mathbb{R} \setminus \mathbb{N}_0)) =  \mu(\emptyset) =0
\end{aligned}
$$

so $\mu_{3s} \perp \nu$. 

&nbsp;&nbsp;&nbsp;&nbsp; Now note that $\nu(A) = 0$ whenever $A \subset (\infty, 0)$ or $A \subset (\mathbb{R} \setminus \mathbb{N})$. When $A \subset (\infty, 0)$, $\mu_{3a}(A) = \mu_3(A \cap \mathbb{N}) = \mu(\emptyset) = 0$. Similarly, when $A \subset (\mathbb{R} \setminus \mathbb{N})$, $\nu(A) = \mu_{3a}(A) = \mu_3(A \cap \mathbb{N}) = 0$. Therefore,

$$
\begin{aligned}
\nu(A) = 0 \implies \mu_{3a}(A) = 0.
\end{aligned}
$$

### Radon-Nikodym derivative

&nbsp;&nbsp;&nbsp;&nbsp; From our work above, note that $\mu_{3a} = \mu_2 \ll \mu_c^+$ and $\nu \ll \mu_c^+$. Moreover, recall that since $\frac{d\nu}{d\mu_c^+} > 0$ a.e.($\mu_c^+$), then $\mu_c^+ \ll \nu$. Therefore,

$$
\begin{aligned}
\frac{d\mu_{3a}}{d\nu} = \frac{d\mu_2}{d\nu} 
&= \frac{d\mu_2}{d\mu_c^+} \left(\frac{d\nu}{d\mu_c^+} \right)^{-1}\\[10pt]
&= \left(\sum_{i = 0}^{\infty} 0.3(1 - 0.3)^i \boldsymbol{1}_{\{i\}}(x)\right)
   \left(\sum_{i = 0}^{\infty}\frac{8^ie^{-8}}{i!} \boldsymbol{1}_{\{i\}}(x)\right)^{-1}\\[10pt]
   &= \sum_{i = 0}^{\infty} \frac{i!e^80.3(1 - 0.3)^i}{8^i} \boldsymbol{1}_{\{i\}}(x)
   \hspace{20pt} \text{a.e. } (\nu)
\end{aligned}
$$

